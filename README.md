# QEMU

Modified version of QEMU for teaching (M2 SETI Embedded Linux, SE758
Linux Device Drivers...).

## ADXL345

The branch `adxl345` contains an implementation of an
[ADXL345](https://www.analog.com/en/products/adxl345.html).

This implementation is limited:

* It uses a single I2C address: `0x53`
* The sampling frequency is fixed (the register `BW_RATE` is ignored)
* The data format is fixed (the register `DATA_FORMAT` is ignored): 16
  bits signed
* Only one interrupt pin is modeled (the register `INT_MAP` is
  ignored)
* The functions *Tap*, *Double Tap* and *Freefall* are not modeled
  (and the corresponding configuration registers are not modeled)
* The only functioning interrupt source is *Watermark*
* The mode *Trigger* of the FIFO is not modeled

For now, the data are generated using the following algorithm. During
each sampling event, a counter `c` is incremented. The data sampled
during this event are:

* If `c % 2 == 0`, `(X,Y,Z) = (c * 4, c * 4 + 1, c * 4 + 2)`
* If `c % 2 != 0`, `(X,Y,Z) = (-(c * 4), -(c * 4 + 1), -(c * 4 + 2))`

This accelerometer is instantiated in the ARM Versatile Express
board. It is connected to the I2C bus. Its interrupt pin is connected
to the pin 50 of the PIC.

To be used by the Linux Kernel, the following modification of the
Device Tree (`arch/arm/boot/dts/vexpress-v2p-ca9.dts`) can be used:

```
&v2m_i2c_dvi {
      adxl345: adxl345@53 {
          compatible = "ad,adxl345";
          reg = <0x53>;
          interrupt-parent = <&gic>;
          interrupts = <0 50 4>;
      };
};
```
